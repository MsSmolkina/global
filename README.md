# Базы данных

- #### [Инструкция по настройке докера (Postgres + pgAdmin + Docker)](Practice_2023/docker/quickstart)

### Материалы курса 2023

- #### [Семинары](Practice_2023/seminars)

### Материалы курса 2024

- #### [Лекции](Lectures)

- #### [Семинары](Practice_2024)

- #### [Домашние задания](Homework)